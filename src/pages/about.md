---
layout: ../layouts/AboutLayout.astro
title: "About"
---

My name is Trevor Opiyo. I'm a backend developer based in Texas.

<div>
  <img src="/assets/logo.png" class="sm:w-1/2 mx-auto" alt="Trevor Opiyo's profile photo">
</div>

Hi I'm Trevor. Subscribe to [my rss feed,](/rss.xml) and follow me across the web via my social links below.
