---
author: Trevor Opiyo
pubDatetime: 2024-01-28T04:59:04.866Z
title: Trevor Opiyo's Annual Media Diet
slug: my-media-diet
featured: true
draft: false
tags:
  - media-diet
description: Trevor Opiyo's Media Diet in 2024.
---

##

One my favorite categories of youtube video are ["What's on my homescreen videos"](https://www.youtube.com/watch?v=NZIArJBACoo&pp=ygUidHlsZXIgc3RhbG1hbiB3aGF0J3Mgb24gbXkgaXBob25lIA%3D%3D). They feel like a tour of someone's house for people on the internet. As decentralized social media continues to [grow](https://fedidb.org) I've seen seen the concept [evolve](https://nathanwentworth.co) (or revert depending on your perspective). I couldn't help but get in on the fun myself. So here's [my media diet.]("https://gitlab.com/trevoropiyo/media_diet") I can't wait to check in next year.
